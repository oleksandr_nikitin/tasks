import { Injectable } from '@angular/core';
import {NotificationComponent} from '../../components/notification/notification.component';
import {DOMService} from './dom.service';
import {ListTasksInterface} from '../../components/list-tasks/list-tasks.interface';

@Injectable()

export class TasksService {
	constructor(
		private domService: DOMService
	) {
	}

	private getRandomArbitrary(): number {
		return Math.random() * (10000 - 5000) + 5000;
	}

	public tasks: Array<ListTasksInterface> = JSON.parse(localStorage.getItem('tasks')) || [];

	public saveTask(data): void {
		const interval = this.getRandomArbitrary();
		const tasks = JSON.parse(localStorage.getItem('tasks')) || [];
		const notificationComponent = this.domService.appendComponentToBody(NotificationComponent);
		notificationComponent.instance.title = data.title;
		tasks.push(data);
		setTimeout(() => {
			localStorage.setItem('tasks', JSON.stringify(tasks));
			this.domService.removeComponentFromBody(notificationComponent);
		}, interval);
	}

	public updateTask(id, tasks): void {
		const interval = this.getRandomArbitrary();
		const notificationComponent = this.domService.appendComponentToBody(NotificationComponent);
		notificationComponent.instance.title = `Update task by ID#${id}`;
		setTimeout(() => {
			localStorage.setItem('tasks', JSON.stringify(tasks));
			this.domService.removeComponentFromBody(notificationComponent);
		}, interval);
	}

	public deleteTask(id, tasks): void {
		const interval = this.getRandomArbitrary();
		const notificationComponent = this.domService.appendComponentToBody(NotificationComponent);
		notificationComponent.instance.title = `Delete task by ID#${id}`;
		setTimeout(() => {
			localStorage.setItem('tasks', JSON.stringify(tasks));
			this.domService.removeComponentFromBody(notificationComponent);
		}, interval);
	}
}
