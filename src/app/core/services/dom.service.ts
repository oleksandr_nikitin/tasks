import {Injectable, ComponentFactoryResolver, ApplicationRef, Injector, EmbeddedViewRef, ComponentRef} from '@angular/core';

@Injectable()

export class DOMService {
	constructor(
		private componentFactoryResolver: ComponentFactoryResolver,
		private applicationRef: ApplicationRef,
		private injector: Injector,
	) {
	}

	appendComponentToBody(component: any): any {
		const componentRef = this.componentFactoryResolver.resolveComponentFactory(component).create(this.injector);
		this.applicationRef.attachView(componentRef.hostView);
		const domElem = (componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
		document.body.appendChild(domElem);
		document.getElementById('notificationContainer').appendChild(domElem);

		return componentRef;
	}

	removeComponentFromBody(componentRef: ComponentRef<any>): void {
		this.applicationRef.detachView(componentRef.hostView);
		componentRef.destroy();
	}
}
