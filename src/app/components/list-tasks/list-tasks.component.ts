import {Component, OnInit} from '@angular/core';
import {ListTasksInterface} from './list-tasks.interface';
import {Router} from '@angular/router';
import {TasksService} from '../../core/services/tasks.service';

@Component({
	selector: 'app-list-tasks',
	templateUrl: 'list-tasks.component.html',
	styleUrls: ['list-tasks.component.sass']
})

export class ListTasksComponent implements OnInit {
	public tasks: Array<ListTasksInterface>;
	constructor(
		private router: Router,
		private tasksService: TasksService
	){}

	onInit(): void {
		this.tasks = this.tasksService.tasks;
		this.tasks.sort((a, b) => {
			if (a.title > b.title) {
				return 1;
			}
			if (a.title < b.title) {
				return -1;
			}
			return 0;
		}).reverse();
		console.log(this.tasks);
	}

	ngOnInit(): void {
		this.onInit();
	}

	add(): void {
		this.router.navigate(['/add']);
	}

	updateTask(id: number): void {
		const taskIndex = this.tasks.findIndex(task => task.id === id);
		this.tasks[taskIndex].complete = true;
		this.tasksService.updateTask(id, this.tasks);
	}

	deleteTask(id: number): void {
		const taskIndex = this.tasks.findIndex(task => task.id === id);
		if (taskIndex > -1) {
			this.tasks.splice(taskIndex, 1);
			this.tasksService.deleteTask(id, this.tasks);
		}
	}
}
