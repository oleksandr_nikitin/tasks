export interface ListTasksInterface {
	id: number;
	title: string;
	description: string;
	date: Date;
	complete: boolean;
}
