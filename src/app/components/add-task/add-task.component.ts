import {Component, OnInit} from '@angular/core';
import {ListTasksInterface} from '../list-tasks/list-tasks.interface';
import {TasksService} from '../../core/services/tasks.service';
import {DOMService} from '../../core/services/dom.service';
import {NotificationComponent} from '../notification/notification.component';
import {Router} from '@angular/router';

@Component({
	selector: 'app-add-task',
	templateUrl: 'add-task.component.html',
	styleUrls: ['add-task.component.sass']
})

export class AddTaskComponent implements OnInit {
	public tasks: Array<ListTasksInterface> = [];
	public task: ListTasksInterface = {
		id: 0,
		title: '',
		description: '',
		date: new Date(),
		complete: false
	};
	constructor(
		private domService: DOMService,
		private tasksService: TasksService,
		private router: Router
	){}

	ngOnInit(): void {
		this.tasks = this.tasksService.tasks;
		this.task.id = this.tasks.length + 1;
	}

	updateData(event: Event, key: string): void {
		const target = event.target as HTMLInputElement;
		this.task[key] = target.value;
	}

	close(): void {
		this.router.navigate(['/']);
	}

	onSaveTask(): void {
		this.tasks.push(this.task);
		this.tasksService.saveTask(this.task);
		this.router.navigate(['/']);
	}
}
