import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ListTasksComponent} from './components/list-tasks/list-tasks.component';
import {AddTaskComponent} from './components/add-task/add-task.component';

const routes: Routes = [
	{
		path: '',
		component: ListTasksComponent
	},
	{
		path: 'add',
		component: AddTaskComponent
	}
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes)
	],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
