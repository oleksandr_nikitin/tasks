import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MatIconModule} from '@angular/material/icon';
import {DOMService} from './core/services/dom.service';
import {TasksService} from './core/services/tasks.service';
import {ListTasksComponent} from './components/list-tasks/list-tasks.component';
import {CommonModule} from '@angular/common';
import {AddTaskComponent} from './components/add-task/add-task.component';

@NgModule({
	declarations: [
		AppComponent,
		ListTasksComponent,
		AddTaskComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		MatIconModule,
		CommonModule
	],
	providers: [
		DOMService,
		TasksService
	],
	bootstrap: [AppComponent]
})
export class AppModule {
}
